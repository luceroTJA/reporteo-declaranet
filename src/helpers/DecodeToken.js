import jwt_decode from "jwt-decode";

const DecodeToken = (user) => {
  let decoded = jwt_decode(user.token);
  return decoded;
};

export default DecodeToken;
