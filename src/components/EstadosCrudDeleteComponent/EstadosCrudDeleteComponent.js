import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import { AuthContext } from "../../auth/authContext";
import { useFormik } from "formik";
import TextField from "@mui/material/TextField";
import * as yup from "yup";
import Button from "@mui/material/Button";
import { insertDificultad, updateDificultad } from "../../API/ApiDificultades";
import { updateEstado } from "../../API/ApiEstados";
import { toast } from "react-toastify";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import FormHelperText from "@mui/material/FormHelperText";

const validationSchema = yup.object({
  nombre: yup
    .string("Ingresa Nombre")
    .max(30, "Menor a 50 caracteres")
    .required("Es necesario"),
  tipo: yup
    .string("Ingresa Color")
    .max(50, "Color menor a 50 caracteres")
    .required("Es necesario"),
});

export const EstadosCrudDeleteComponent = (props) => {
  const { user } = React.useContext(AuthContext);
  const [disabled, setDisabled] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      id_estado: props.crudAction.data.id_estado,
      nombre: props.crudAction.data.nombre,
      tipo: props.crudAction.data.tipo,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (values.tipo === "Seleccionar") {
        toast.error("Debes seleccionar un tipo");
      } else {
        setDisabled(true);
        updateEstado(user, values).then((resultado) => {
          if (resultado.status == 200) {
            setDisabled(false);
            props.setCrudAction({ action: null, data: null });
          } else {
            setDisabled(false);
          }
        });
      }
    },
  });

  //Llamando funciones
  const escFunction = (event) => {
    if (event.keyCode === 27) {
      props.setCrudAction({ action: null, data: null });
    }
  };

  //Efectos
  React.useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);

  return (
    <div className="crudDivComponent col-md-5 px-4">
      <div className="container">
        <div className="row">
          <div className="col-12 my-2 mb-5">
            <div className="d-flex justify-content-between align-items-center">
              <div>
                <h5>Dificultad</h5>
                <small>
                  {props.crudAction.action == "delete" ? (
                    <>Eliminar</>
                  ) : (
                    <>Editar</>
                  )}
                </small>
              </div>
              <div
                className="btn btn-sm btn-danger"
                onClick={() => {
                  props.setCrudAction({ action: null, data: null });
                }}
              >
                <CloseIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <form onSubmit={formik.handleSubmit}>
              <TextField
                autoFocus
                fullWidth
                id="nombre"
                name="nombre"
                label="Nombre Estado"
                className="mb-2"
                size="small"
                value={formik.values.nombre}
                disabled={true}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                helpertext={formik.touched.nombre && formik.errors.nombre}
              />
              <FormControl fullWidth variant="standard">
                <InputLabel id="tipo">Tipo</InputLabel>
                <Select
                  labelId="tipo"
                  id="tipo"
                  value={formik.values.tipo}
                  label="Tipo"
                  disabled={true}
                  error={formik.touched.tipo && Boolean(formik.errors.tipo)}
                  onChange={(e) => {
                    formik.setFieldValue("tipo", e.target.value);
                  }}
                >
                  <MenuItem value={"Seleccionar"}>Seleccionar</MenuItem>
                  <MenuItem value={"Expediente"}>Expediente</MenuItem>
                  <MenuItem value={"Promoción"}>Promoción</MenuItem>
                  <MenuItem value={"Sentencia"}>Sentencia</MenuItem>
                </Select>
                <FormHelperText>
                  {formik.touched.tipo && formik.errors.tipo}
                </FormHelperText>
              </FormControl>

              <Button
                color="error"
                variant="contained"
                fullWidth
                type="submit"
                className="mt-3"
                disabled={disabled}
              >
                Eliminar
              </Button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
