import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import { AuthContext } from "../../auth/authContext";
import { useFormik } from "formik";
import TextField from "@mui/material/TextField";
import * as yup from "yup";
import Button from "@mui/material/Button";
import { insertDificultad } from "../../API/ApiDificultades";
import { insertEstatus } from "../../API/ApiEstatus";

const validationSchema = yup.object({
  nombre: yup
    .string("Ingresa Nombre")
    .max(50, "Menor a 50 caracteres")
    .required("Es necesario"),
});

export const EstatusCrudAddComponent = (props) => {
  const { user } = React.useContext(AuthContext);
  const [disabled, setDisabled] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      nombre: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      setDisabled(true);
      insertEstatus(user, values).then((resultado) => {
        if (resultado.status == 200) {
          props.setCrudAction({ action: null, data: null });
          setDisabled(false);
        } else {
          setDisabled(false);
        }
      });
      debugger;
    },
  });

  //Llamando funciones
  const escFunction = (event) => {
    if (event.keyCode === 27) {
      props.setCrudAction({ action: null, data: null });
    }
  };

  //Efectos
  React.useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);

  return (
    <div className="crudDivComponent col-md-5 px-4">
      <div className="container">
        <div className="row">
          <div className="col-12 my-2 mb-5">
            <div className="d-flex justify-content-between align-items-center">
              <div>
                <h5>{props.nombre}Estatus</h5>
                <small>Agregar</small>
              </div>
              <div
                className="btn btn-sm btn-danger"
                onClick={() => {
                  props.setCrudAction({ action: null, data: null });
                }}
              >
                <CloseIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <form onSubmit={formik.handleSubmit}>
              <TextField
                autoFocus
                fullWidth
                id="nombre"
                name="nombre"
                label="Nombre Estatus"
                className="mb-2"
                size="small"
                value={formik.values.nombre}
                disabled={disabled}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                helperText={formik.touched.nombre && formik.errors.nombre}
              />

              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                className="mt-3"
                disabled={disabled}
              >
                Agregar {props.nombre}
              </Button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
