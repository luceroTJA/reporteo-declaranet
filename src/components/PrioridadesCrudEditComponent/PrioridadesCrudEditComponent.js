import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import { AuthContext } from "../../auth/authContext";
import { useFormik } from "formik";
import TextField from "@mui/material/TextField";
import * as yup from "yup";
import Button from "@mui/material/Button";
import { insertDificultad, updateDificultad } from "../../API/ApiDificultades";
import { updateEstado } from "../../API/ApiEstados";
import { toast } from "react-toastify";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import FormHelperText from "@mui/material/FormHelperText";
import { updatePrioridad } from "../../API/ApiPrioridades";

const validationSchema = yup.object({
  nombre: yup
    .string("Ingresa Nombre")
    .max(30, "Menor a 30 caracteres")
    .required("Es necesario"),
  color: yup
    .string("Ingresa Nombre")
    .max(30, "Menor a 30 caracteres")
    .required("Es necesario"),
  valor: yup
    .string("Ingresa Nombre")
    .max(30, "Menor a 30 caracteres")
    .required("Es necesario"),
});

export const PrioridadesCrudEditComponent = (props) => {
  const { user } = React.useContext(AuthContext);
  const [disabled, setDisabled] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      id_prioridad: props.crudAction.data.id_prioridad,
      nombre: props.crudAction.data.nombre,
      color: props.crudAction.data.color,
      valor: props.crudAction.data.valor,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (values.tipo === "Seleccionar") {
        toast.error("Debes seleccionar un tipo");
      } else {
        setDisabled(true);
        updatePrioridad(user, values).then((resultado) => {
          if (resultado.status == 200) {
            setDisabled(false);
            props.setCrudAction({ action: null, data: null });
          } else {
            setDisabled(false);
          }
        });
      }
    },
  });

  //Llamando funciones
  const escFunction = (event) => {
    if (event.keyCode === 27) {
      props.setCrudAction({ action: null, data: null });
    }
  };

  //Efectos
  React.useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);

  return (
    <div className="crudDivComponent col-md-5 px-4">
      <div className="container">
        <div className="row">
          <div className="col-12 my-2 mb-5">
            <div className="d-flex justify-content-between align-items-center">
              <div>
                <h5>Dificultad</h5>
                <small>
                  {props.crudAction.action == "delete" ? (
                    <>Eliminar</>
                  ) : (
                    <>Editar</>
                  )}
                </small>
              </div>
              <div
                className="btn btn-sm btn-danger"
                onClick={() => {
                  props.setCrudAction({ action: null, data: null });
                }}
              >
                <CloseIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <form onSubmit={formik.handleSubmit}>
              <TextField
                autoFocus
                fullWidth
                id="nombre"
                name="nombre"
                label="Nombre Prioridad"
                className="mb-2"
                size="small"
                value={formik.values.nombre}
                disabled={disabled}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                helpertext={formik.touched.nombre && formik.errors.nombre}
              />
              <TextField
                fullWidth
                id="color"
                name="color"
                type="color"
                label="Color"
                className="mb-2"
                size="small"
                value={formik.values.color}
                disabled={disabled}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.color && Boolean(formik.errors.color)}
                helpertext={formik.touched.color && formik.errors.color}
              />
              <TextField
                fullWidth
                id="valor"
                name="valor"
                type={"number"}
                label="Nombre Valor"
                className="mb-2"
                size="small"
                value={formik.values.valor}
                disabled={disabled}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.valor && Boolean(formik.errors.valor)}
                helpertext={formik.touched.valor && formik.errors.valor}
              />

              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                className="mt-3"
                disabled={disabled}
              >
                Editar {props.nombre}
              </Button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
