import React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

const years = [
  { anio: "2013" },
  { anio: "2014" },
  { anio: "2015" },
  { anio: "2016" },
  { anio: "2017" },
  { anio: "2018" },
  { anio: "2019" },
  { anio: "2020" },
  { anio: "2021" },
  { anio: "2022" },
  { anio: "2023" },
];

export const SearchBarAnioProyectoComponent = () => {
  const [anios, setAnios] = React.useState(years);

  const [anioSeleccionado, setAnioSeleccionado] = React.useState("");
  const handleChange = (event) => {
    setAnioSeleccionado(event.target.value);
  };

  React.useEffect(() => {
    const localDate = new Date();
    let currentYear = localDate.getFullYear().toString();
    setAnioSeleccionado(currentYear);
  }, []);

  return (
    <React.Fragment>
      <div className="w-100">
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Ano</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={anioSeleccionado}
            size="small"
            label="Selecciona el año"
            onChange={handleChange}
          >
            {anios.map((anioLista, index) => (
              <MenuItem key={index} value={anioLista.anio}>
                {anioLista.anio}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    </React.Fragment>
  );
};
