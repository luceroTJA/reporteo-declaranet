import React from "react";
import { SketchPicker, BlockPicker, GithubPicker } from "react-color";
import CircleIcon from "@mui/icons-material/Circle";
import SquareIcon from '@mui/icons-material/Square';
const popover = {
  zIndex: "5000",
  position: 'absolute'
};

const colors = ["#74B4FF", "#84630E", "#FD6E6E"];

export const SemaforoColorComponent = (props) => {
  const [colorEtiqueta, setColorEtiqueta] = React.useState(
    props.colorPredeterminado
  );
  const [mostrarEtiquetaColor, setMostrarEtiquetaColor] = React.useState(false);

  return (
    <React.Fragment>
      <SquareIcon
        fontSize="large"
        className="position-relative"
        style={{ color: colorEtiqueta }}
        onClick={() => {
          /*Toggle de Colores*/
          setMostrarEtiquetaColor(!mostrarEtiquetaColor);
        }}
      />

      {mostrarEtiquetaColor ? (
        <div style={popover}>
          <div
            onClick={() => {
              setMostrarEtiquetaColor(false);
            }}
          />
          <GithubPicker
            color={colorEtiqueta}
            colors={colors}
            onChangeComplete={(e) => {
              setColorEtiqueta(e.hex);
              setMostrarEtiquetaColor(false);
            }}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};
