import React from "react";
import styles from "./EtiquetaColorComponent.module.css";
import { SketchPicker, BlockPicker, GithubPicker } from "react-color";
const popover = {
  position: "relative",
  top: 0,
  left: 0,
  zIndex: "2",
};
const cover = {
  position: "fixed",
  top: "0px",
  right: "0px",
  bottom: "0px",
  left: "0px",
};

export const EtiquetaColorComponent = (props) => {
  const [colorEtiqueta, setColorEtiqueta] = React.useState(
    props.colorPredeterminado
  );
  const [mostrarEtiquetaColor, setMostrarEtiquetaColor] = React.useState(false);

  return (
    <React.Fragment>
      <div
        className={`position-absolute top-50 start-0 translate-middle ${styles.triangulo}`}
        style={{ borderLeftColor: colorEtiqueta }}
        onClick={() => {
          /*Toggle de Colores*/
          setMostrarEtiquetaColor(!mostrarEtiquetaColor);
        }}
      ></div>

      {mostrarEtiquetaColor ? (
        <div style={popover}>
          <div
            style={cover}
            onClick={() => {
              setMostrarEtiquetaColor(false);
            }}
          />
          <GithubPicker
            color={colorEtiqueta}
            onChangeComplete={(e) => {
              setColorEtiqueta(e.hex);
              setMostrarEtiquetaColor(false);
            }}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};
