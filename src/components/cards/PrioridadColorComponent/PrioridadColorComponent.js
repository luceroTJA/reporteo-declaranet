import React from "react";
import {
  SketchPicker,
  BlockPicker,
  GithubPicker,
  CirclePicker,
} from "react-color";
import CircleIcon from "@mui/icons-material/Circle";
import { toast } from "react-toastify";
const popover = {
  zIndex: "5000",
  position: "absolute",
};

const colors = ["#74B4FF", "#84630E", "#FD6E6E"];

export const PrioridadColorComponent = (props) => {
  const [colorEtiqueta, setColorEtiqueta] = React.useState(
    props.colorPredeterminado
  );
  const [mostrarEtiquetaColor, setMostrarEtiquetaColor] = React.useState(false);

  return (
    <React.Fragment>
      <CircleIcon
        fontSize="large"
        className="position-relative"
        style={{ color: colorEtiqueta }}
        onClick={() => {
          setMostrarEtiquetaColor(!mostrarEtiquetaColor);
        }}
      />
      {mostrarEtiquetaColor ? (
        <div style={popover}>
          <GithubPicker
            color={colorEtiqueta}
            colors={colors}
            onChangeComplete={(e) => {
              setColorEtiqueta(e.hex);
              setMostrarEtiquetaColor(false);
            }}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};
