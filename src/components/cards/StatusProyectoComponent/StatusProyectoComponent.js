import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import styles from "./StatusProyectoComponent.module.css";
import { toast } from "react-toastify";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { inputAdornmentClasses } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";

const listadoTemasJSON = [
  { tare_id: 1, tare_descripcion: "Pendiente" },
  { tare_id: 2, tare_descripcion: "Trabajando" },
  { tare_id: 3, tare_descripcion: "Revisión" },
  { tare_id: 4, tare_descripcion: "Suspendido" },
  { tare_id: 5, tare_descripcion: "Returnado" },
  { tare_id: 6, tare_descripcion: "Validaddo" },
  {
    tare_id: 7,
    tare_descripcion: "Terminado",
  },
];

export const StatusProyectoComponent = () => {
  const [editVisibility, setEditVisibility] = React.useState(false);
  const [listadoTemas, setListadoTemas] = React.useState(listadoTemasJSON);
  const [nombreTema, setNombreTema] = React.useState("Pendiente");

  const handleChange = (event) => {};

  return (
    <React.Fragment>
      {editVisibility === false ? (
        <div
          className="d-flex flex-column w-100"
          onClick={() => setEditVisibility(true)}
        >
          <div
            className={styles.temaTexto}
            onClick={() => {
              toast.success("Cambiar Tema de proyecto");
            }}
          >
            <EditIcon fontSize="inherit" className={styles.editIcon} />{" "}
            <span style={{ fontSize: 12 }}>{nombreTema}</span>
          </div>
        </div>
      ) : (
        <div className="d-flex flex-column w-100">
          <div className={styles.temaTexto}>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={listadoTemas}
              clearOnEscape={true}
              getOptionLabel={(option) => `${option.tare_descripcion}`}
              onKeyDown={(event, option) => {
                if (event.code === "Escape") {
                  setEditVisibility(false);
                }
                /* searchContracts(user, event.target.value).then((resultado) => {
                  if (resultado.status == 200) {
                    setFinalData(resultado.data);
                  } else {
                    setFinalData([]);
                  }
                }); */
              }}
              onChange={(event, option) => {
                setNombreTema(option.tare_descripcion);
                setEditVisibility(false);
              }}
              sx={{ width: "100%" }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  autoFocus={true}
                  variant="standard"
                  size="small"
                  sx={{ fontSize: 12 }}
                  label="Cambiar Status"
                />
              )}
            />
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
