import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import styles from "./TemaProyectosComponent.module.css";
import { toast } from "react-toastify";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { inputAdornmentClasses } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";

const listadoTemasJSON = [
  { tare_id: 1, tare_descripcion: "ACCESO A LA INFORMACIÓN PÚBLICA" },
  { tare_id: 2, tare_descripcion: "ACUERDOS DE AYUNTAMIENTO" },
  { tare_id: 3, tare_descripcion: "AGUA POTABLE" },
  { tare_id: 4, tare_descripcion: "ALCOHOLES" },
  { tare_id: 5, tare_descripcion: "CONTRATOS" },
  { tare_id: 6, tare_descripcion: "CONTRIBUCIÓN DE MEJORAS" },
  {
    tare_id: 7,
    tare_descripcion:
      "CRÉDITO FISCAL Y/O PROCEDIMIENTO ADMINISTRATIVO DE EJECUCIÓN",
  },
  { tare_id: 8, tare_descripcion: "DERECHO NOTARIAL" },
  { tare_id: 9, tare_descripcion: "DERECHO REGISTRAL" },
  {
    tare_id: 10,
    tare_descripcion: "DESTITUCIÓN DE ELEMENTOS DE SEGURIDAD PÚBLICA",
  },
  {
    tare_id: 11,
    tare_descripcion:
      "DESTITUCIÓN DE SERVIDORES PÚBLICOS FUERA DE P.A.D. O P.R.A.",
  },
  { tare_id: 12, tare_descripcion: "DEVOLUCIÓN DE PAGO DE LO INDEBIDO" },
  { tare_id: 13, tare_descripcion: "EDUCACIÓN" },
  { tare_id: 14, tare_descripcion: "EXPROPIACIÓN" },
  { tare_id: 15, tare_descripcion: "IMPUESTO PREDIAL" },
  { tare_id: 16, tare_descripcion: "INDEFINIDO O NO SEÑALADO" },
  { tare_id: 17, tare_descripcion: "JUICIO DE LESIVIDAD" },
  { tare_id: 18, tare_descripcion: "LICENCIAS DE MANEJO" },
  { tare_id: 19, tare_descripcion: "LICENCIAS, PERMISOS O AUTORIZACIONES" },
  { tare_id: 20, tare_descripcion: "LICITACIONES PÚBLICAS" },
  {
    tare_id: 21,
    tare_descripcion: "MULTAS ADMINISTRATIVAS DIVERSAS A TRÁNSITO Y TRANSPORTE",
  },
  { tare_id: 22, tare_descripcion: "MULTAS DE TRÁNSITO Y TRANSPORTE" },
  { tare_id: 23, tare_descripcion: "NEGATIVAS FICTAS" },
  { tare_id: 24, tare_descripcion: "OTROS" },
  { tare_id: 25, tare_descripcion: "PAGO DE PRESTACIONES SALARIALES" },
  { tare_id: 26, tare_descripcion: "RESOLUCIÓN DE RECURSOS ADMINISTRATIVOS" },
  { tare_id: 27, tare_descripcion: "RESPONSABILIDAD ADMINISTRATIVA" },
  { tare_id: 28, tare_descripcion: "RESPONSABILIDAD PATRIMONIAL (R.P.)" },
  { tare_id: 29, tare_descripcion: "SEGURIDAD SOCIAL (I.S.S.E.G.)" },
  { tare_id: 30, tare_descripcion: "SERVICIO PÚBLICO DE TRANSPORTE" },
  {
    tare_id: 31,
    tare_descripcion: "VISITAS DE INSPECCIÓN Y CLAUSURAS DIVERSAS A ALCOHOLES",
  },
  { tare_id: 32, tare_descripcion: "DECLARATORIA DE BENEFICIARIOS" },
];

export const TemaProyectosComponent = () => {
  const [editVisibility, setEditVisibility] = React.useState(false);
  const [listadoTemas, setListadoTemas] = React.useState(listadoTemasJSON);
  const [nombreTema, setNombreTema] = React.useState("Boleta de Infracción");

  const handleChange = (event) => {
    setNombreTema(event.target.value);
    setEditVisibility(false);
  };

  return (
    <React.Fragment>
      {editVisibility === false ? (
        <div
          className="d-flex flex-column w-100"
          onClick={() => setEditVisibility(true)}
        >
          <div
            className={styles.temaTexto}
            onClick={() => {
              toast.success("Cambiar Tema de proyecto");
            }}
          >
            <EditIcon fontSize="inherit" className={styles.editIcon} />{" "}
            <span style={{ fontSize: 12 }}>{nombreTema}</span>
          </div>
        </div>
      ) : (
        <div className="d-flex flex-column w-100">
          <div className={styles.temaTexto}>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              clearOnEscape={true}
              options={listadoTemas}
              getOptionLabel={(option) => `${option.tare_descripcion}`}
              onKeyDown={(event, option) => {
                if ((event.code === "Escape")) {
                  setEditVisibility(false);
                }
                /* searchContracts(user, event.target.value).then((resultado) => {
                  if (resultado.status == 200) {
                    setFinalData(resultado.data);
                  } else {
                    setFinalData([]);
                  }
                }); */
              }}
              onChange={(event, option) => {
                setNombreTema(option.tare_descripcion);
                setEditVisibility(false);
              }}
              sx={{ width: "100%" }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size="small"
                  variant="standard"
                  autoFocus={true}
                  sx={{ fontSize: 12 }}
                  label="Buscar Tema"
                />
              )}
            />
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
