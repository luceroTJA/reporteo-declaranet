import React from "react";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import styles from "./ActionsProyectoComponent.module.css";
import Popup from "reactjs-popup";
export const ActionsProyectoComponent = () => {
  return (
    <div>
      <Popup
        trigger={
          <span
            className={`${styles.disableSelect}`}
            style={{ cursor: "pointer" }}
          >
            <MoreVertIcon className={styles.moreIcon} />
          </span>
        }
        position="bottom right"
      >
        <ul
          className="list-group list-group-flush"
          style={{ fontSize: "13px" }}
        >
          <li
            className={
              "list-group-item list-group-item-action " + styles.disableSelect
            }
            onClick={() => {
              window.open("https://intranet.tjagto.gob.mx/", "_blank");
            }}
          >
            Ir a Intranet
          </li>
          <li
            className={
              "list-group-item list-group-item-action " + styles.disableSelect
            }
            onClick={() => {
              window.open(
                "https://serviciosdigitales.tjagto.gob.mx/tribunalelectronicoweb/Dashboard.aspx",
                "_blank"
              );
            }}
          >
            Sistema Expedientes
          </li>
          <li
            className={
              "list-group-item list-group-item-action ps-4 " +
              styles.disableSelect
            }
            onClick={() => {
              /* dispatch({
                        type: types.logout,
                      });*/
              window.location.href = "/";
            }}
          >
            Cerrar Sesión
          </li>
        </ul>
      </Popup>
    </ div>
  );
};
