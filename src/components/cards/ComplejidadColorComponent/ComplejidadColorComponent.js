import React from "react";
import { SketchPicker, BlockPicker, GithubPicker } from "react-color";
import CircleIcon from "@mui/icons-material/Circle";
import SquareIcon from "@mui/icons-material/Square";
import TripOriginIcon from "@mui/icons-material/TripOrigin";
const popover = {
  zIndex: "5000",
  position: 'absolute'
};

const colors = ["#4caf50", "#ffeb3b", "#ff9800", "#f44336"];

export const ComplejidadColorComponent = (props) => {
  const [colorEtiqueta, setColorEtiqueta] = React.useState(
    props.colorPredeterminado
  );
  const [mostrarEtiquetaColor, setMostrarEtiquetaColor] = React.useState(false);

  return (
    <React.Fragment>
      <TripOriginIcon
        fontSize="large"
        className="position-relative"
        style={{ color: colorEtiqueta }}
        onClick={() => {
          /*Toggle de Colores*/
          setMostrarEtiquetaColor(!mostrarEtiquetaColor);
        }}
      />

      {mostrarEtiquetaColor ? (
        <div style={popover}>
          <div
            onClick={() => {
              setMostrarEtiquetaColor(false);
            }}
          />
          <GithubPicker
            color={colorEtiqueta}
            colors={colors}
            onChangeComplete={(e) => {
              setColorEtiqueta(e.hex);
              setMostrarEtiquetaColor(false);
            }}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};
