import { color } from "@mui/system";
import React from "react";

import ChangeHistoryIcon from "@mui/icons-material/ChangeHistory";
import styles from "./CardProyectoComponent.module.css";
import { EtiquetaColorComponent } from "../EtiquetaColorComponent/EtiquetaColorComponent";
import { ExpedienteYTipoComponent } from "../ExpedienteYTipoComponent/ExpedienteYTipoComponent";
import { PrioridadColorComponent } from "../PrioridadColorComponent/PrioridadColorComponent";
import { SemaforoColorComponent } from "../SemaforoColorComponent/SemaforoColorComponent";
import { ComplejidadColorComponent } from "../ComplejidadColorComponent/ComplejidadColorComponent";
import { TemaProyectosComponent } from "../TemaProyectosComponent/TemaProyectosComponent";
import { PersonaProyectosComponent } from "../PersonaProyectosComponent/PersonaProyectosComponent";
import { FechaTurnoProyectoComponent } from "../FechaTurnoProyectoComponent/FechaTurnoProyectoComponent";
import { ActionsProyectoComponent } from "../ActionsProyectoComponent/ActionsProyectoComponent";
import { StatusProyectoComponent } from "../StatusProyectoComponent/StatusProyectoComponent";

export const CardProyectoComponent = () => {
  return (
    <div className="col-12 gx-0 bg-white rounded mb-3 position-relative pt-2 pb-2 ">
      <EtiquetaColorComponent colorPredeterminado="#adadad" />
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-2 d-flex align-items-center text-center">
            <ExpedienteYTipoComponent />
          </div>
          <div className="col-md-2 d-flex align-items-center position-relative">
            <PrioridadColorComponent colorPredeterminado="#adadad" />
            <SemaforoColorComponent colorPredeterminado="#adadad" />
            <ComplejidadColorComponent colorPredeterminado="#adadad" />
          </div>
          <div className="col-md-2 d-flex d-flex-column align-items-center">
            <TemaProyectosComponent />
          </div>
          <div className="col-md-2 d-flex align-items-center">
            <PersonaProyectosComponent />
          </div>

          <div className="col-md-1 d-flex align-items-center">
            <FechaTurnoProyectoComponent />
          </div>
          <div className="col-md-2 d-flex align-items-center">
            <StatusProyectoComponent />
          </div>
          <div className="position-absolute top-50 start-100">
            <ActionsProyectoComponent />
          </div>
        </div>
      </div>
    </div>
  );
};
