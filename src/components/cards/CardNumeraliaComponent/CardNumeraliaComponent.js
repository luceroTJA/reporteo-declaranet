import React from "react";
import styles from "./CardNumeraliaComponent.module.css";
export const CardNumeraliaComponent = (props) => {
  return (
    <div
      className={`card cursor-pointer`}
      style={
        props.tarjeta.isActive === true
          ? {
              backgroundColor: props.tarjeta.colorActivo,
              color: props.tarjeta.fontColor,
            }
          : { backgroundColor: props.tarjeta.colorInactivo, color: "black" }
      }
      onClick={() => {
        let items = [...props.listadoPrioridad];
        items.map((tarjetita) =>
          tarjetita.id === props.tarjeta.id
            ? (tarjetita.isActive = !tarjetita.isActive)
            : tarjetita.isActive
        );
        props.setListadoPrioridad(items);
      }}
    >
      <div className="card-body d-flex align-items-center d-flex justify-content-between py-3">
        <small className={`p-0 m-0 fw-normal ${styles.textoComponente}`}>{props.tarjeta.nombre}</small>
        <span className={`p-0 m-0 ${styles.textoComponente}`}>
          {props.tarjeta.numero}
        </span>
      </div>
    </div>
  );
};
