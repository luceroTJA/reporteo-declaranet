import React from "react";
import { CambiarResponsableProyectoComponent } from "../CambiarResponsableProyectoComponent/CambiarResponsableProyectoComponent";
import { TituloTarjetaProyectoComponent } from "../TituloTarjetaProyectoComponent/TituloTarjetaProyectoComponent";

export const DatosProyectoSistemaExpedientesComponent = () => {
  return (
    <React.Fragment>
      <div className="mt-2">
        <ul class="list-group list-group-flush bg-transparent">
          <li class="list-group-item flex-column bg-transparent">
            <strong>Expediente PA Sumario 500/2022</strong> <br />
            <small>Expediente</small>
          </li>
          <li class="list-group-item flex-column bg-transparent">
            <strong>Cuarta Sala</strong> <br />
            <small>Sala</small>
          </li>
          <li class="list-group-item flex-column bg-transparent">
            <strong>Fecha de Sentencia</strong> <br />
            <small>00/00/0000</small>
          </li>
        </ul>
      </div>
      <div className="mt-3">
        <TituloTarjetaProyectoComponent titulo="Responsable / Corresponsable" />
        <ul class="list-group list-group-flush bg-transparent">
          <li class="list-group-item flex-column bg-transparent">
           <CambiarResponsableProyectoComponent />
          </li>
          <li class="list-group-item flex-column bg-transparent">
            <strong>Carlos Francisco Estrada G</strong> <br />
            <small>Corresponsable</small>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
};
