import React from "react";

export const TituloTarjetaProyectoComponent = (props) => {
  return props.titulo ? (
    <React.Fragment>
      <h6 className="p-0 m-0 mb-2" style={{color: '#0F208A'}}>{props.titulo}</h6>
      <hr className="p-0 m-0" />
    </React.Fragment>
  ) : null;
};
