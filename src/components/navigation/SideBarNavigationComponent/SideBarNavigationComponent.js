import React from "react";
import styles from "./SideBarNavigationComponent.module.css";
import AssignmentIcon from "@mui/icons-material/Assignment";
import AssessmentIcon from "@mui/icons-material/Assessment";
import WorkspacesIcon from "@mui/icons-material/Workspaces";
import SupportAgentIcon from "@mui/icons-material/SupportAgent";
import HomeIcon from "@mui/icons-material/Home";
import { useNavigate } from "react-router-dom";
import SettingsIcon from "@mui/icons-material/Settings";

const menuPrincipal = [
  {
    tituloMenu: "Acciones",
    accesosMenu: [
      {
        nombre: "Inicio",
        icono: <HomeIcon fontSize="small" />,
        enlace: "/",
        enlaceExterno: false,
      },
      {
        nombre: "Proyectos",
        icono: <AssignmentIcon fontSize="small" />,
        enlace: "/proyectos",
        enlaceExterno: false,
      },
      {
        nombre: "Reportes",
        icono: <AssessmentIcon fontSize="small" />,
        enlace: "/reportes",
        enlaceExterno: false,
      },
      {
        nombre: "Configuración Días",
        icono: <SettingsIcon fontSize="small" />,
        enlace: "/configuracion",
        enlaceExterno: false,
      },
     
    ],
  },
  {
    tituloMenu: "Configuración",
    accesosMenu: [
      {
        nombre: "Dificultades",
        icono: <SettingsIcon fontSize="small" />,
        enlace: "/dificultades",
        enlaceExterno: false,
      },
      {
        nombre: "Estados",
        icono: <SettingsIcon fontSize="small" />,
        enlace: "/estados",
        enlaceExterno: false,
      },
      {
        nombre: "Estatus",
        icono: <SettingsIcon fontSize="small" />,
        enlace: "/estatus",
        enlaceExterno: false,
      },
      {
        nombre: "Prioridades",
        icono: <SettingsIcon fontSize="small" />,
        enlace: "/prioridades",
        enlaceExterno: false,
      },
    ]
  },
  {
    tituloMenu: "Recursos Adicionales",
    accesosMenu: [
      {
        nombre: "Sistema Tribunal",
        icono: <WorkspacesIcon fontSize="small" />,
        enlace:
          "https://serviciosdigitales.tjagto.gob.mx/tribunalelectronicoweb/Dashboard.aspx",
        enlaceExterno: true,
      },
    ],
  },
  {
    tituloMenu: "Soporte Técnico",
    accesosMenu: [
      {
        nombre: "Service Desk TJA",
        icono: <SupportAgentIcon fontSize="small" />,
        enlace: "https://intranet.tjagto.gob.mx/informatica",
        enlaceExterno: true,
      },
    ],
  },
];

export const SideBarNavigationComponent = (props) => {
  let navigate = useNavigate();

  if (props.menuAbierto == true) {
    return (
      <React.Fragment>
        <div className={styles.sidebarAbierto}>
          {menuPrincipal.map((menu, index) => (
            <React.Fragment key={index}>
              <section className={styles.menuDivider}>
                <span className={styles.menuDividerSpan}>
                  {menu.tituloMenu}{" "}
                </span>
              </section>
              {menu.accesosMenu.map((acceso, index) =>
                acceso.enlaceExterno === true ? (
                  <div
                    key={index}
                    onClick={() => {
                      window.location.href = acceso.enlace;
                    }}
                    className={`d-flex flex-row d-flex align-items-center ps-3 cursor-pointer p-1  ${styles.menuSelectionOpen}`}
                  >
                    <span className="pe-2">{acceso.icono}</span>
                    <span style={{ fontSize: "14px" }}>{acceso.nombre}</span>
                  </div>
                ) : (
                  <div
                    key={index}
                    onClick={() => {
                      navigate(acceso.enlace);
                    }}
                    className={`d-flex flex-row d-flex align-items-center ps-3 cursor-pointer p-1  ${styles.menuSelectionOpen}`}
                  >
                    <span className="pe-2">{acceso.icono}</span>
                    <span style={{ fontSize: "14px" }}>{acceso.nombre}</span>
                  </div>
                )
              )}
            </React.Fragment>
          ))}
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <div className={styles.sidebarCerrado}>
          {menuPrincipal.map((menu, index) => (
            <React.Fragment key={index}>
              {menu.accesosMenu.map((acceso, index) =>
                acceso.enlaceExterno === true ? (
                  <div
                    key={index}
                    onClick={() => {
                      window.location.href = acceso.enlace;
                    }}
                    className={`d-flex flex-row d-flex align-items-center ps-3 cursor-pointer p-1  ${styles.menuSelectionOpen}`}
                  >
                    <span className="pe-2">{acceso.icono}</span>
                  </div>
                ) : (
                  <div
                    key={index}
                    onClick={() => {
                      navigate(acceso.enlace);
                    }}
                    className={`d-flex flex-row d-flex align-items-center ps-3 cursor-pointer p-1  ${styles.menuSelectionOpen}`}
                  >
                    <span className="pe-2">{acceso.icono}</span>
                  </div>
                )
              )}
            </React.Fragment>
          ))}
        </div>
      </React.Fragment>
    );
  }
};
