import React from "react";
import Popup from "reactjs-popup";
import Badge, { BadgeProps } from "@mui/material/Badge";
import NotificationsIcon from "@mui/icons-material/Notifications";
import styles from "./NotificationComponent.module.css";
export const NotificationComponent = () => {
  return (
    <Popup
      trigger={
        <span
          className={`${styles.disableSelect}`}
          style={{ cursor: "pointer" }}
        >
          <Badge badgeContent={4} color="secondary">
            <NotificationsIcon style={{ color: "#8BC21E" }} />
          </Badge>
        </span>
      }
      position="bottom right"
    >
      <ul className="list-group list-group-flush" style={{ fontSize: "13px" }}>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
        >
          <span>Notificación título</span> <br />
          <small>
            <strong> Hace x Minutos</strong>
          </small>
        </li>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
        >
          <span>Notificación título</span> <br />
          <small>
            <strong> Hace x Minutos</strong>
          </small>
        </li>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
        >
          <span>Notificación título</span> <br />
          <small>
            <strong> Hace x Minutos</strong>
          </small>
        </li>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
        >
          <span>Notificación título</span> <br />
          <small>
            <strong> Hace x Minutos</strong>
          </small>
        </li>
      </ul>
    </Popup>
  );
};
