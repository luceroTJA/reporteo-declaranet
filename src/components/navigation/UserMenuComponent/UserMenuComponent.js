import React from "react";
import Popup from "reactjs-popup";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import styles from "./UserMenuComponent.module.css";
import imagenIntranet from "../../../images/intranetLogo.png";
import logoTJA from "../../../images/tjaicon.png";
import { AuthContext } from "../../../auth/authContext";
import DecodeToken from "../../../helpers/DecodeToken";
export const UserMenuComponent = () => {
  const { user } = React.useContext(AuthContext);

  console.log(DecodeToken(user));

  const [resultadoUsuario, setResultadoUsuario] = React.useState(
    DecodeToken(user)
  );

  return (
    <Popup
      trigger={
        <span
          className={`${styles.disableSelect}`}
          style={{ cursor: "pointer" }}
        >
          <small className="fw-normal">
            {resultadoUsuario.first_name} {resultadoUsuario.last_name}
          </small>
          <KeyboardArrowDownIcon fontSize="small" />
          <br></br>
          <small className={`fw-light me-4 ${styles.puesto}`}>
            {resultadoUsuario.subarea}
          </small>
        </span>
      }
      position="bottom right"
    >
      <ul className="list-group list-group-flush" style={{ fontSize: "13px" }}>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
          onClick={() => {
            window.open("https://intranet.tjagto.gob.mx/", "_blank");
          }}
        >
          <img
            alt="Abrir Intranet"
            src={imagenIntranet}
            height={15}
            className="pe-2"
          />
          Ir a Intranet
        </li>
        <li
          className={
            "list-group-item list-group-item-action " + styles.disableSelect
          }
          onClick={() => {
            window.open(
              "https://serviciosdigitales.tjagto.gob.mx/tribunalelectronicoweb/Dashboard.aspx",
              "_blank"
            );
          }}
        >
          <img
            alt="Abrir Intranet"
            src={logoTJA}
            height={15}
            className="pe-2"
          />
          Sistema Expedientes
        </li>
        <li
          className={
            "list-group-item list-group-item-action ps-4 " +
            styles.disableSelect
          }
          onClick={() => {
            /* dispatch({
                        type: types.logout,
                      });*/
            window.location.href = "/";
          }}
        >
          Cerrar Sesión
        </li>
      </ul>
    </Popup>
  );
};
