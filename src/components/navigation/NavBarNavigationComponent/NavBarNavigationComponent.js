import React from "react";
import styles from "./NavBarNavigationComponent.module.css";
import Skeleton from "@mui/material/Skeleton";
import imagenImagotipo from "../../../images/tjaimagotipo.png";
import hamburguerIcon from "../../../images/Hamburguer_Menu.png";
import { NotificationComponent } from "../NotificationComponent/NotificationComponent";
import { UserMenuComponent } from "../UserMenuComponent/UserMenuComponent";
import MenuIcon from "@mui/icons-material/Menu";
export const NavBarNavigationComponent = (props) => {
  const [loaded, setLoaded] = React.useState(true);
  return loaded === true ? (
    <div
      className={`d-flex justify-content-between ${styles.menuPrincipal} d-flex align-items-center`}
    >
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3 d-flex justify-content-between d-flex align-items-center">
            <MenuIcon
              className={`${styles.menuHamburguesa}`}
              onClick={() => {
                let valorActual = props.menuAbierto;

                props.setMenuAbierto(!valorActual);

                localStorage.setItem("menuOpen", JSON.stringify(!valorActual));
              }}
            />

            <span
              onClick={() => {
                window.location.href = "/";
              }}
            >
              Monitor TJA
            </span>
          </div>
          <div className="col-md-6 text-center fw-normal d-flex align-items-center d-flex justify-content-center">
            <span
              onClick={() => {
                window.location.reload();
              }}
            >
              {props.pageName ? props.pageName : "Sin nombre de ventana"}
            </span>
          </div>
          <div className="col-md-3 text-end d-flex align-items-center d-flex justify-content-end">
            <span className="me-4">
              <NotificationComponent />
            </span>
            <span>
              <UserMenuComponent />
            </span>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className={`d-flex justify-content-between ${styles.menuPrincipal}`}>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-2">
            <Skeleton animation="wave" />
          </div>
          <div className="col-md-8 text-center fw-light">
            <Skeleton animation={false} />
          </div>
          <div className="col-md-2 text-end">
            {" "}
            <Skeleton animation={false} />
          </div>
        </div>
      </div>
    </div>
  );
};
