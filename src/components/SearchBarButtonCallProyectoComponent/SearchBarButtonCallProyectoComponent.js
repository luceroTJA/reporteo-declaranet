import React from "react";
import Button from "@mui/material/Button";
import UpdateIcon from "@mui/icons-material/Update";
import { ProyectoContext } from "../../proyectosContext/proyectoContext";
import { typesProyecto } from "./../../types/typesProyecto";

export const SearchBarButtonCallProyectoComponent = () => {
  const { proyecto, dispatchProyecto } = React.useContext(ProyectoContext);

  debugger;
  return (
    <Button
      fullWidth
      variant="contained"
      onClick={() => {
        dispatchProyecto({
          type: typesProyecto.agregarInformacion,
          payload: { dato: "22", cambia: true },
        });
      }}
    >
      <UpdateIcon />
    </Button>
  );
};
