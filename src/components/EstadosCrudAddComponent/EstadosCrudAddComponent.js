import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import { AuthContext } from "../../auth/authContext";
import { useFormik } from "formik";
import TextField from "@mui/material/TextField";
import * as yup from "yup";
import Button from "@mui/material/Button";
import { insertDificultad } from "../../API/ApiDificultades";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { toast } from "react-toastify";
import FormHelperText from "@mui/material/FormHelperText";
import { insertEstado } from "../../API/ApiEstados";

const validationSchema = yup.object({
  nombre: yup
    .string("Ingresa Nombre")
    .max(30, "Menor a 30 caracteres")
    .required("Es necesario"),
  tipo: yup
    .string("Ingresa Tipo")
    .max(50, "Tipo menor a 50 caracteres")
    .required("Es necesario"),
});

export const EstadosCrudAddComponent = (props) => {
  const { user } = React.useContext(AuthContext);
  const [disabled, setDisabled] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      nombre: "",
      tipo: "Seleccionar",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      debugger;
      if (values.tipo === "Seleccionar") {
        toast.error("Debes seleccionar un tipo");
      } else {
        setDisabled(true);
        insertEstado(user, values).then((resultado) => {
          if (resultado.status == 200) {
            setDisabled(false);
            props.setCrudAction({ action: null, data: null });
          } else {
            setDisabled(false);
          }
        });
      }
    },
  });

  //Llamando funciones
  const escFunction = (event) => {
    if (event.keyCode === 27) {
      props.setCrudAction({ action: null, data: null });
    }
  };

  //Efectos
  React.useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);

  return (
    <div className="crudDivComponent col-md-5 px-4">
      <div className="container">
        <div className="row">
          <div className="col-12 my-2 mb-5">
            <div className="d-flex justify-content-between align-items-center">
              <div>
                <h5>{props.nombre}Estado</h5>
                <small>Agregar</small>
              </div>
              <div
                className="btn btn-sm btn-danger"
                onClick={() => {
                  props.setCrudAction({ action: null, data: null });
                }}
              >
                <CloseIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <form onSubmit={formik.handleSubmit}>
              <TextField
                autoFocus
                fullWidth
                id="nombre"
                name="nombre"
                label="Nombre Estado"
                className="mb-2"
                size="small"
                value={formik.values.nombre}
                disabled={disabled}
                variant="standard"
                onChange={formik.handleChange}
                error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                helpertext={formik.touched.nombre && formik.errors.nombre}
              />
              <FormControl fullWidth variant="standard">
                <InputLabel id="tipo">Tipo</InputLabel>
                <Select
                  labelId="tipo"
                  id="tipo"
                  value={formik.values.tipo}
                  label="Tipo"
                  disabled={disabled}
                  error={formik.touched.tipo && Boolean(formik.errors.tipo)}
                  onChange={(e) => {
                    formik.setFieldValue("tipo", e.target.value);
                  }}
                >
                  <MenuItem value={"Seleccionar"}>Seleccionar</MenuItem>
                  <MenuItem value={"Expediente"}>Expediente</MenuItem>
                  <MenuItem value={"Promoción"}>Promoción</MenuItem>
                  <MenuItem value={"Sentencia"}>Sentencia</MenuItem>
                </Select>
                <FormHelperText>
                  {formik.touched.tipo && formik.errors.tipo}
                </FormHelperText>
              </FormControl>

              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                className="mt-3"
                disabled={disabled}
              >
                Agregar Estado
              </Button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
