import React from "react";
import { Routes, Route } from "react-router-dom";
import { ConfiguracionDeDiasScreenComponent } from "../screens/ConfiguracionDeDiasScreenComponent/ConfiguracionDeDiasScreenComponent";
import { DetalleProyectoScreenComponent } from "../screens/DetalleProyectoScreenComponent/DetalleProyectoScreenComponent";
import { DificultadesCrudScreenComponent } from "../screens/DificultadesCrudScreenComponent/DificultadesCrudScreenComponent";
import { EstadosCrudScreenComponent } from "../screens/EstadosCrudScreenComponent/EstadosCrudScreenComponent";
import { InicioScreenComponent } from "../screens/InicioScreenComponent/InicioScreenComponent";
import { PaginaNoEncontradaScreenComponent } from "../screens/PaginaNoEncontradaScreenComponent/PaginaNoEncontradaScreenComponent";
import { PrioridadesCrudScreenComponent } from "../screens/PrioridadesCrudScreenComponent/PrioridadesCrudScreenComponent";
import { ProyectosScreenComponent } from "../screens/ProyectosScreenComponent/ProyectosScreenComponent";
import { ReportesScreenComponent } from "../screens/ReportesScreenComponent/ReportesScreenComponent";
import { StatusCrudScreenComponent } from "../screens/StatusCrudScreenComponent/StatusCrudScreenComponent";
import { AppContainer } from "./AppContainer";

export const DashboardRoutes = () => {
  return (
    <>
      <Routes>
        <Route path="*" element={<PaginaNoEncontradaScreenComponent />} />
        <Route
          path="/"
          element={
            <>
              <AppContainer pageName="Panel Principal">
                <InicioScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="dificultades"
          element={
            <>
              <AppContainer pageName="Catálogo de Dificultades">
                <DificultadesCrudScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="estados"
          element={
            <>
              <AppContainer pageName="Catálogo de Estados">
                <EstadosCrudScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="estatus"
          element={
            <>
              <AppContainer pageName="Catálogo de Status">
                <StatusCrudScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="prioridades"
          element={
            <>
              <AppContainer pageName="Catálogo de Status">
                <PrioridadesCrudScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="proyectos"
          element={
            <>
              <AppContainer pageName="Panel Principal">
                <ProyectosScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="proyecto/:idproyecto"
          element={
            <>
              <AppContainer pageName="Detalle Proyecto">
                <DetalleProyectoScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="reportes"
          element={
            <>
              <AppContainer pageName="Panel Principal">
                <ReportesScreenComponent />
              </AppContainer>
            </>
          }
        />
        <Route
          path="configuracion"
          element={
            <>
              <AppContainer pageName="Configuración de Días">
                <ConfiguracionDeDiasScreenComponent />
              </AppContainer>
            </>
          }
        />
      </Routes>
    </>
  );
};
