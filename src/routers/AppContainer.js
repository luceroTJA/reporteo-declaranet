import React from "react";
import { NavBarNavigationComponent } from "../components/navigation/NavBarNavigationComponent/NavBarNavigationComponent";
import { SideBarNavigationComponent } from "../components/navigation/SideBarNavigationComponent/SideBarNavigationComponent";
import styles from "./AppContainer.module.css";
export const AppContainer = (props) => {
  const [menuAbierto, setMenuAbierto] = React.useState(
    JSON.parse(localStorage.getItem("menuOpen"))
  );

  const hola = JSON.parse(localStorage.getItem("menuOpen"));

  return (
    <React.Fragment>
      <NavBarNavigationComponent
        menuAbierto={menuAbierto}
        pageName={props.pageName}
        setMenuAbierto={setMenuAbierto}
      />
      <SideBarNavigationComponent
        menuAbierto={menuAbierto}
        setMenuAbierto={setMenuAbierto}
      />

      <div
        className={styles.mainContainer}
        style={menuAbierto == true ? { left: "230px" } : { left: "50px" }}
      >
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-12">{props.children}</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
