import React from "react";
import AddIcon from "@mui/icons-material/Add";

import { getAllEstados } from "../../API/ApiEstados";
import { AuthContext } from "../../auth/authContext";
import { EstadosCrudAddComponent } from "../../components/EstadosCrudAddComponent/EstadosCrudAddComponent";
import { EstadosCrudEditComponent } from "../../components/EstadosCrudEditComponent/EstadosCrudEditComponent";
import { EstadosCrudDeleteComponent } from "../../components/EstadosCrudDeleteComponent/EstadosCrudDeleteComponent";
import { getAllPrioridades } from "../../API/ApiPrioridades";
import { PrioridadesCrudAddComponent } from "../../components/PrioridadesCrudAddComponent/PrioridadesCrudAddComponent";
import { PrioridadesCrudEditComponent } from "../../components/PrioridadesCrudEditComponent/PrioridadesCrudEditComponent";
import { PrioridadesCrudDeleteComponent } from "../../components/PrioridadesCrudDeleteComponent/PrioridadesCrudDeleteComponent";

export const PrioridadesCrudScreenComponent = () => {
  const { user, dispatch } = React.useContext(AuthContext);
  const [finalData, setFinalData] = React.useState([]);
  const [crudAction, setCrudAction] = React.useState({
    action: null,
    data: null,
  });

  React.useEffect(() => {
    getAllPrioridades(user, null).then((resultado) => {
      if (resultado.status == 200) {
        setFinalData(resultado.data);
      }
    });
  }, [crudAction]);

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <header className="mb-4 mt-3 border-bottom d-flex justify-content-between">
              <span className="fs-5">Catálogo de Prioridades</span>
              <button
                type="button"
                className="btn btn-sm btn-success mb-2"
                onClick={() => {
                  setCrudAction({
                    action: "add",
                    data: null,
                  });
                }}
              >
                <AddIcon fontSize="inherit" /> Agregar
              </button>
            </header>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row row-cols-md-3">
          {finalData.map((estado) => (
            <React.Fragment key={estado.id_prioridad}>
              <div className="col mb-2">
                <div className="card" style={{ backgroundColor: estado.color }}>
                  <div className="card-body">
                    <strong>{estado.nombre}</strong> <br /> Color:{" "}
                    {estado.color} <br /> Valor: {estado.valor}
                  </div>
                  <div className="card-footer">
                    <div
                      className="btn btn-sm btn-success me-xl-1"
                      onClick={() => {
                        setCrudAction({
                          action: "edit",
                          data: estado,
                        });
                      }}
                    >
                      Editar
                    </div>
                    <div
                      className="btn btn-sm btn-danger"
                      onClick={() => {
                        setCrudAction({
                          action: "delete",
                          data: estado,
                        });
                      }}
                    >
                      Eliminar
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
      {crudAction.action == "add" ? (
        <>
          <PrioridadesCrudAddComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />
        </>
      ) : null}
      {crudAction.action == "edit" ? (
        <>
          {" "}
          <PrioridadesCrudEditComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}

      {crudAction.action == "delete" ? (
        <>
          {" "}
          <PrioridadesCrudDeleteComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}
    </>
  );
};
