import React from "react";
import AddIcon from "@mui/icons-material/Add";
import { AuthContext } from "./../../auth/authContext";
import { getAllDificultades } from "../../API/ApiDificultades";
import { DificultadesCrudAddComponent } from "../../components/DificultadesCrudAddComponent/DificultadesCrudAddComponent";
import { DificultadesCrudEditComponent } from "../../components/DificultadesCrudEditComponent/DificultadesCrudEditComponent";
import { DificultadesCrudDeleteComponent } from "../../components/DificultadesCrudDeleteComponent/DificultadesCrudDeleteComponent";
export const DificultadesCrudScreenComponent = () => {
  //Trayendo usuario
  const { user, dispatch } = React.useContext(AuthContext);

  const [finalData, setFinalData] = React.useState([]);

  const [crudAction, setCrudAction] = React.useState({
    action: null,
    data: null,
  });

  React.useEffect(() => {
    getAllDificultades(user, null).then((resultado) => {
      if (resultado.status == 200) {
        setFinalData(resultado.data);
      }
    });
  }, [crudAction]);

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <header className="mb-4 mt-3 border-bottom d-flex justify-content-between">
              <span className="fs-5">Catálogo de Dificultades</span>
              <button
                type="button"
                className="btn btn-sm btn-success mb-2"
                onClick={() => {
                  setCrudAction({
                    action: "add",
                    data: null,
                  });
                }}
              >
                <AddIcon fontSize="inherit" /> Agregar
              </button>
            </header>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row row-cols-md-3">
          {finalData.map((dificultad) => (
            <React.Fragment key={dificultad.id_dificultad}>
              <div className="col mb-2">
                <div
                  className="card"
                  style={{ backgroundColor: dificultad.color, color: "white" }}
                >
                  <div className="card-body">{ dificultad.nombre} <br /> Valor: {dificultad.valor}</div>
                  <div className="card-footer">
                    <div
                      className="btn btn-sm btn-success me-xl-1"
                      onClick={() => {
                        setCrudAction({
                          action: "edit",
                          data: dificultad,
                        });
                      }}
                    >
                      Editar
                    </div>
                    <div
                      className="btn btn-sm btn-danger d-none"
                      onClick={() => {
                        setCrudAction({
                          action: "delete",
                          data: dificultad,
                        });
                      }}
                    >
                      Eliminar
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
      {crudAction.action == "add" ? (
        <>
          {" "}
          <DificultadesCrudAddComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}
      {crudAction.action == "edit" ? (
        <>
          {" "}
          <DificultadesCrudEditComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}
      {crudAction.action == "delete" ? (
        <>
          {" "}
          <DificultadesCrudDeleteComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}
    </>
  );
};
