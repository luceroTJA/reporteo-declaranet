import React from "react";
import { DatosProyectoSistemaExpedientesComponent } from "../../components/DetalleProyectoComponent/DatosProyectoSistemaExpedientesComponent/DatosProyectoSistemaExpedientesComponent";
import { TituloTarjetaProyectoComponent } from "../../components/DetalleProyectoComponent/TituloTarjetaProyectoComponent/TituloTarjetaProyectoComponent";
import styles from "./DetalleProyectoScreenComponent.module.css";
export const DetalleProyectoScreenComponent = () => {
  return (
    <div className="container-fluid">
      <div className="row ">
        <div className="col-md-9">
          <div
            className={`card p-3 cursor-pointer`}
            style={{
              backgroundColor: "#F5F5F7",
              height: "calc(100vh - 80px)",
              maxHeight: "calc(100vh - 80px)",
              minHeight: "calc(100vh - 80px)",
            }}
          >
            <TituloTarjetaProyectoComponent titulo="Información de Proyecto" />
          </div>
        </div>
        <div className="col-md-3">
          <div
            className={`card p-3 cursor-pointer`}
            style={{
              backgroundColor: "#F5F5F7",
              height: "calc(100vh - 80px)",
              maxHeight: "calc(100vh - 80px)",
              minHeight: "calc(100vh - 80px)",
            }}
          >
            <TituloTarjetaProyectoComponent titulo="Datos Sistema Expedientes"/>
            <DatosProyectoSistemaExpedientesComponent />
          </div>
        </div>
      </div>
    </div>
  );
};
