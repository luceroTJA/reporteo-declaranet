import React from "react";
import AddIcon from "@mui/icons-material/Add";

import { getAllEstados } from "../../API/ApiEstados";
import { AuthContext } from "../../auth/authContext";
import { EstadosCrudAddComponent } from "../../components/EstadosCrudAddComponent/EstadosCrudAddComponent";
import { EstadosCrudEditComponent } from "../../components/EstadosCrudEditComponent/EstadosCrudEditComponent";
import { EstadosCrudDeleteComponent } from "../../components/EstadosCrudDeleteComponent/EstadosCrudDeleteComponent";

export const EstadosCrudScreenComponent = () => {
  const { user, dispatch } = React.useContext(AuthContext);
  const [finalData, setFinalData] = React.useState([]);
  const [crudAction, setCrudAction] = React.useState({
    action: null,
    data: null,
  });

  React.useEffect(() => {
    getAllEstados(user, null).then((resultado) => {
      if (resultado.status == 200) {
        setFinalData(resultado.data);
      }
    });
  }, [crudAction]);

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <header className="mb-4 mt-3 border-bottom d-flex justify-content-between">
              <span className="fs-5">Catálogo de Estados</span>
              <button
                type="button"
                className="btn btn-sm btn-success mb-2"
                onClick={() => {
                  setCrudAction({
                    action: "add",
                    data: null,
                  });
                }}
              >
                <AddIcon fontSize="inherit" /> Agregar
              </button>
            </header>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row row-cols-md-3">
          {finalData.map((estado) => (
            <React.Fragment key={estado.id_estado}>
              <div className="col mb-2">
                <div className="card">
                  <div className="card-body">
                    <strong>{estado.nombre}</strong> <br /> Tipo: {estado.tipo}
                  </div>
                  <div className="card-footer">
                    <div
                      className="btn btn-sm btn-success me-xl-1"
                      onClick={() => {
                        setCrudAction({
                          action: "edit",
                          data: estado,
                        });
                      }}
                    >
                      Editar
                    </div>
                    <div
                      className="btn btn-sm btn-danger d-none"
                      onClick={() => {
                        setCrudAction({
                          action: "delete",
                          data: estado,
                        });
                      }}
                    >
                      Eliminar
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
      {crudAction.action == "add" ? (
        <>
          <EstadosCrudAddComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />
        </>
      ) : null}
      {crudAction.action == "edit" ? (
        <>
          {" "}
          <EstadosCrudEditComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}

      {crudAction.action == "delete" ? (
        <>
          {" "}
          <EstadosCrudDeleteComponent
            crudAction={crudAction}
            setCrudAction={setCrudAction}
          />{" "}
        </>
      ) : null}
    </>
  );
};
