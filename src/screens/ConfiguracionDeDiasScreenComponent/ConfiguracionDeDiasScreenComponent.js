import React from "react";
import logotja from "../../images/logoTJA.png";
import SettingsIcon from "@mui/icons-material/Settings";
import TextField from "@mui/material/TextField";

export const ConfiguracionDeDiasScreenComponent = () => {
  return (
    <React.Fragment>
      <div className="container-fluid mt-5 mb-2 col-md-6 mx-auto">
        <div className="row">
          <div className="col-md-3">
            <img src={logotja} className="img-fluid" alt="Logotipo TJA" />
          </div>
          <div className="col-md-5"></div>
          <div className="col-md-4 text-end small">
            <strong>
              <SettingsIcon /> Configurar Días
            </strong>
          </div>
        </div>
      </div>
      <div className="container-fluid mt-3 mb-2 col-md-6 mx-auto">
        <div className="row">
          <div className="col-md-3 align-self-end">
            <div>Nombre Semáforo</div>
          </div>
          <div className="col-md-3">
            <div>
              <TextField
                size="small"
                id="standard-basic"
                label="Standard"
                variant="standard"
                type={"number"}
              />
            </div>
          </div>
          <div className="col-md-3">
            <div>
              <TextField
                size="small"
                id="standard-basic"
                label="Standard"
                variant="standard"
                type={"number"}
              />
            </div>
          </div>
          <div className="col-md-3">
            <div>
              <TextField
                size="small"
                id="standard-basic"
                label="Standard"
                variant="standard"
                type={"number"}
              />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
