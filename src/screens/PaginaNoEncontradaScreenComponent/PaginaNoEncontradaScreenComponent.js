import React from "react";
import styles from "./PaginaNoEncontradaScreenComponent.module.css";
import Button from "@mui/material/Button";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ErrorIcon from "@mui/icons-material/Error";
export const PaginaNoEncontradaScreenComponent = () => {
  return (
    <React.Fragment>
      <div className="position-absolute top-50 start-50 translate-middle text-center">
        <h1>
          <ErrorIcon fontSize="large" style={{ color: "red" }} />
        </h1>
        <h2 className="text-center center-text fw-semibold">404</h2>
        <h5 className="text-center center-text fw-light">
          No pudimos encontrar <br />
          la página que estas buscando
        </h5>
        <br />
        <button
          type="button"
          onClick={() => {
            window.location.href = "/";
          }}
          className="centet-text text-center btn btn-primary btn-sm"
        >
          <ArrowBackIcon /> Regresar
        </button>
      </div>
    </React.Fragment>
  );
};
