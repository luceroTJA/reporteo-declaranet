import React from "react";
import { CardNumeraliaComponent } from "../../components/cards/CardNumeraliaComponent/CardNumeraliaComponent";
import { CardProyectoComponent } from "../../components/cards/CardProyectoComponent/CardProyectoComponent";
import { SearchBarAnioProyectoComponent } from "../../components/SearchBarAnioProyectoComponent/SearchBarAnioProyectoComponent";
import { SearchBarButtonCallProyectoComponent } from "../../components/SearchBarButtonCallProyectoComponent/SearchBarButtonCallProyectoComponent";
import { SearchBarProyectoComponent } from "../../components/SearchBarProyectoComponent/SearchBarProyectoComponent";

const listadoPrioridadCards = [
  {
    id: 1,
    nombre: "Baja",
    numero: 50,
    colorActivo: "#74B4FF",
    fontColor: "white",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 2,
    nombre: "Media",
    numero: 20,
    colorActivo: "#84630E",
    fontColor: "white",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 3,
    nombre: "Alta",
    numero: 10,
    colorActivo: "#FD6E6E",
    fontColor: "white",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
];

const listadoStatusProyectoCards = [
  {
    id: 1,
    nombre: "Pendiente",
    numero: 50,
    colorActivo: "rgba(255,61,236,0.15)",
    fontColor: "#FF3DEC",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 2,
    nombre: "Trabajando",
    numero: 20,
    colorActivo: "rgba(0,170,223,0.15)",
    fontColor: "#00AADF",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 3,
    nombre: "Revisión",
    numero: 10,
    colorActivo: "rgba(242,129,23,0.15)",
    fontColor: "#F28117",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 4,
    nombre: "Suspendido",
    numero: 10,
    colorActivo: "rgba(239,12,12,0.15)",
    fontColor: "#EF0C0C",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 5,
    nombre: "Returnado",
    numero: 10,
    colorActivo: "rgba(15,32,138,0.15)",
    fontColor: "#0F208A",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 6,
    nombre: "Validado",
    numero: 10,
    colorActivo: "rgba(139,194,30,0.15)",
    fontColor: "#8BC21E",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 7,
    nombre: "Terminado",
    numero: 10,
    colorActivo: "rgba(139,194,30,0.15)",
    fontColor: "#8BC21E",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
];
const listadoTipoExpedienteCards = [
  {
    id: 1,
    nombre: "Tradicional",
    numero: 50,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 2,
    nombre: "Trad. Sumario",
    numero: 20,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 3,
    nombre: "J.L. Ordinario",
    numero: 10,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 4,
    nombre: "J.L. Sumario",
    numero: 10,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 5,
    nombre: "Apelaciones",
    numero: 10,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 6,
    nombre: "TOCAS",
    numero: 10,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
  {
    id: 7,
    nombre: "RR",
    numero: 10,
    colorActivo: "rgba(0,0,0,0.2)",
    fontColor: "black",
    colorInactivo: "rgba(245,245,245,0.5)",
    isActive: true,
  },
];

export const ProyectosScreenComponent = () => {
  const [listadoPrioridad, setListadoPrioridad] = React.useState(
    listadoPrioridadCards
  );
  const [listadoStatus, setListadoStatus] = React.useState(
    listadoStatusProyectoCards
  );
  const [listadoExpedientes, setListadoExpedientes] = React.useState(
    listadoTipoExpedienteCards
  );

  return (
    <>
      <div className="container-fluid mt-2">
        <div className="row row-cols-7">
          <div className="col mb-2 text-center mt-2">
            <small>Prioridad</small>
          </div>
          <div className="col mb-2">
            <CardNumeraliaComponent
              listadoPrioridad={listadoPrioridad}
              tarjeta={listadoPrioridad[0]}
              setListadoPrioridad={setListadoPrioridad}
            />
          </div>
          <div className="col mb-2">
            <CardNumeraliaComponent
              listadoPrioridad={listadoPrioridad}
              tarjeta={listadoPrioridad[1]}
              setListadoPrioridad={setListadoPrioridad}
            />
          </div>
          <div className="col mb-2">
            <CardNumeraliaComponent
              listadoPrioridad={listadoPrioridad}
              tarjeta={listadoPrioridad[2]}
              setListadoPrioridad={setListadoPrioridad}
            />
          </div>

          <div className="col mb-2 text-center mt-2">
            <small>Selecciona Fechas</small>
          </div>
          <div className="col mb-2">
            <SearchBarProyectoComponent />
          </div>

          <div className="col mb-2">
            <SearchBarAnioProyectoComponent />
          </div>

          <div className="col mb-2">
            <SearchBarButtonCallProyectoComponent />
          </div>
        </div>
      </div>
      <div className="container-fluid mt-2">
        <div className="row row-cols-7">
          <div className="col mb-2 text-center mt-2">
            <small>Status Proyecto</small>
          </div>
          {listadoStatus.map((tarjeta) => (
            <React.Fragment key={tarjeta.id}>
              <div className="col mb-2">
                <CardNumeraliaComponent
                  listadoPrioridad={listadoStatus}
                  tarjeta={tarjeta}
                  setListadoPrioridad={setListadoStatus}
                />
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
      <div className="container-fluid mt-2">
        <div className="row row-cols-7">
          <div className="col mb-2 text-center mt-2">
            <small>Expediente</small>
          </div>
          {listadoExpedientes.map((tarjeta) => (
            <React.Fragment key={tarjeta.id}>
              <div className="col mb-2">
                <CardNumeraliaComponent
                  listadoPrioridad={listadoExpedientes}
                  tarjeta={tarjeta}
                  setListadoPrioridad={setListadoExpedientes}
                />
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>

      <div className="container-fluid pe-4 ps-4 pt-3 pb-2">
        <div className="row">
          <div
            className="col-12 gx-0 bg-white rounded position-relative"
            style={{ fontSize: 12 }}
          >
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-2"></div>
                <div className="col-md-2"></div>
                <div className="col-md-2">Tema</div>
                <div className="col-md-2">Persona</div>
                <div className="col-md-1">Fecha</div>
                <div className="col-md-2">Status</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-light p-4 rounded">
        <div className="row">
          <CardProyectoComponent />
          <CardProyectoComponent />
          <CardProyectoComponent />
          <CardProyectoComponent />
          <CardProyectoComponent />
          <CardProyectoComponent />
        </div>
      </div>
    </>
  );
};
