import React from "react";
import imageProyectos from "../../images/proyectosIcon.png";
import imageReportes from "../../images/reportesIcon.png";
import styles from "./InicioScreenComponent.module.css";
import { useNavigate } from "react-router-dom";

export const InicioScreenComponent = () => {
  let navigate = useNavigate();

  return (
    <div className="position-absolute top-50 start-50 translate-middle">
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            <div
              className={"card " + styles.card}
              onClick={() => {
                navigate("/proyectos");
              }}
            >
              <div className="card-body">
                <img
                  src={imageProyectos}
                  alt="Imagen Proyectos"
                  width={60}
                  className="mb-3"
                />
                <h5>Proyectos</h5>
                <p>
                  Revisa los proyectos que se te han asignado, accede a
                  documentarlos.
                </p>
              </div>
            </div>
          </div>
          <div className="col">
            <div
              className={"card " + styles.card}
              onClick={() => {
                navigate("/reportes");
              }}
            >
              <div className="card-body">
                <img
                  src={imageReportes}
                  alt="Imagen Reportes"
                  width={60}
                  className="mb-3"
                />
                <h5>Reportes</h5>
                <p>
                  Accede a los reportes de avance de proyectos de una manera
                  rápida y ágil.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
