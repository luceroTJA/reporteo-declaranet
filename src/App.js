import React from "react";
import { useEffect, useReducer } from "react";
import { AuthContext } from "./auth/authContext";
import { authReducer } from "./auth/authReducer";
import { AppRouter } from "./routers/AppRouter";

import "bootstrap/dist/css/bootstrap.css";
import "../src/styles/tjastyle.css";
import "reactjs-popup/dist/index.css";

//importanto Toastify
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Importando Helmet
import { HelmetProvider } from "react-helmet-async";
import { ProyectoContext } from "./proyectosContext/proyectoContext";
import { proyectoReducer } from "./proyectosContext/proyectoReducer";

const init = () => {
  return JSON.parse(localStorage.getItem("user")) || { logged: false };
};

const proyectoInicial = () => {
  return { valores: 123, pruebas: 23, todobien: 123 };
};

function App() {
  const [user, dispatch] = useReducer(authReducer, {}, init);

  const [proyecto, dispatchProyecto] = useReducer(
    proyectoReducer,
    {},
    proyectoInicial
  );

  useEffect(() => {
    if (!user) return;

    localStorage.setItem("user", JSON.stringify(user));
  }, [user]);

  return (
    <React.Fragment>
      <AuthContext.Provider
        value={{
          user,
          dispatch,
        }}
      >
        <ProyectoContext.Provider value={{proyecto, dispatchProyecto}}>
          <HelmetProvider>
            <ToastContainer />
            <AppRouter />
          </HelmetProvider>
        </ProyectoContext.Provider>
      </AuthContext.Provider>
    </React.Fragment>
  );
}

export default App;
