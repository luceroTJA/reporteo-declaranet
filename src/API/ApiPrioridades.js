import axios from "axios";
import { toast } from "react-toastify";

export const getAllPrioridades = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/prioridades";

  try {
    const response = await axios.get(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};

export const insertPrioridad = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/prioridades";

  try {
    const response = await axios.post(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
export const updatePrioridad = async (user, values) => {
  let url =
    process.env.REACT_APP_API_URL_ENDPOINT +
    "monexp/prioridades/" +
    values.id_prioridad;

  try {
    const response = await axios.put(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
