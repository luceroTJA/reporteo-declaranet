import axios from "axios";
import { toast } from "react-toastify";

export const getAllDificultades = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/dificultades";

  try {
    const response = await axios.get(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};

export const insertDificultad = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/dificultades";

  try {
    const response = await axios.post(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
export const updateDificultad = async (user, values) => {
  let url =
    process.env.REACT_APP_API_URL_ENDPOINT +
    "monexp/dificultades/" +
    values.id_dificultad;

    debugger;
  try {
    const response = await axios.put(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
