import axios from "axios";
import { toast } from "react-toastify";

export const getAllEstados = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/estados";

  try {
    const response = await axios.get(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};

export const insertEstado = async (user, values) => {
  let url = process.env.REACT_APP_API_URL_ENDPOINT + "monexp/estados";

  try {
    const response = await axios.post(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
export const updateEstado = async (user, values) => {
  let url =
    process.env.REACT_APP_API_URL_ENDPOINT +
    "monexp/estados/" +
    values.id_estado;

  try {
    const response = await axios.put(url, values, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${user.token}`,
      },
    });
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.error(error);
    return error;
  }
};
